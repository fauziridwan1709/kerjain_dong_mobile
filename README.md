# kerjain_dong

[![Generic badge](https://img.shields.io/badge/Flutter-v2.0.6-blue)](https://flutter.dev/docs) [![Generic badge](https://img.shields.io/badge/Dart-v2.12.2-blue)](https://dart.dev/guides) [![pipeline status](https://gitlab.com/fauziridwan1709/kerjain_dong_mobile/badges/master/pipeline.svg)](https://gitlab.com/fauziridwan1709/kerjain_dong_mobile/-/commits/master) [![coverage report](https://gitlab.com/fauziridwan1709/kerjain_dong_mobile/badges/master/coverage.svg)](https://gitlab.com/fauziridwan1709/kerjain_dong_mobile/-/commits/master) [![Generic badge](https://img.shields.io/badge/release-v1.0.0-brightgreen)](https://play.google.com/store/apps/)

## Getting Started


## How to run apk

```
flutter clean
flutter pub get
flutter run -t lib/main_staging.dart --release --no-shrink
```


## State Management

Using state rebuilder for zero boilerplate state management
visit https://pub.dev/packages/states_rebuilder

### Example

This is the starting point of the application. All the application level configurations are defined in this file i.e, theme, routes, title, orientation etc.

```dart
import 'package:boilerplate/routes.dart';
import 'package:flutter/material.dart';
export 'package:states_rebuilder/states_rebuilder.dart';
import 'package:flutter/services.dart';

import 'constants/app_theme.dart';
import 'constants/strings.dart';
import 'ui/splash/splash.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
  ]).then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Injector(
      inject: [Inject(() => ClassModelState())],
      builder: (context) {
        return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: Strings.appName,
                  theme: themeData,
                  routes: Routes.routes,
                  home: SplashScreen(),
              );
      }
    );
  }
}
```

## Future State

extending FutureState<> as a part of state rebuilder.

```dart
class CourseState implements FutureState<CourseState, K> {
  ///put cacheKey here
  @override
  String cacheKey;
  
  ///put condition here value to notify when the state need to rebuild 
  @override
  bool condition;

  List<CourseDetail> courses;

  @override
  Future<void> retrieveData() async {
    try {
      var course = await ClassServices.getMeCourse();
      courses = course.data;
    } on SocketException {
      throw SocketException('No Internet');
    } on TimeoutException {
      throw TimeoutException('timeout, try again');
    } catch (e) {
      throw SiswamediaException('Terjadi kesalahan');
    }
  }

  @override
  Future<void> createData(Map<String, dynamic> data) {
    // TODO: implement createData
    throw UnimplementedError();
  }

  @override
  Future<void> deleteData(int id) {
    // TODO: implement deleteData
    throw UnimplementedError();
  }

  @override
  Future<void> updateData(int k) {
    // TODO: implement updateData
    throw UnimplementedError();
  }
}
```

## How to test coverage
*flutter test --coverage widget_test && genhtml -o coverage coverage/lcov.info*

## Rules
* use @deprecated before delete and update method or function
* put Reusable widget or global use kind of things inside CORE folder


