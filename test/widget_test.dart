import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:kerjain_dong/core/bases/states/_states.dart';
import 'package:kerjain_dong/core/bases/widgets/text/_text.dart';
import 'package:kerjain_dong/core/utils/_utils.dart';
import 'package:kerjain_dong/features/authentication/data/datasources/_datasources.dart';
import 'package:kerjain_dong/features/authentication/data/models/_models.dart';
import 'package:kerjain_dong/features/authentication/data/repositories/_repositories.dart';
import 'package:kerjain_dong/features/authentication/domain/repositories/_repositories.dart';
import 'package:kerjain_dong/features/home/domain/entities/_entities.dart';
import 'package:kerjain_dong/features/home/presentation/pages/_pages.dart';
import 'package:kerjain_dong/main.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class MockClients extends Mock implements http.Client {}

void main() {
  AuthenticationRepository _repo;
  Widget _widget;
  Widget _widget2;
  Widget _homeWidget;
  Widget _textWidget;

  setUp(() async {
    SharedPreferences.setMockInitialValues(
        {'login': false, 'todoListCacheKey': DateTime.now().toIso8601String()});
    var instance = await StreamingSharedPreferences.instance;
    Pref.init(preferences: instance);
    final mockClient = MockClient((request) async {
      print(request.method);
      if (request.method == 'PUT') {
        return Response('{}', 200);
      } else if (request.method == 'GET') {
        return Response('{}', 200);
      } else if (request.method == 'DELETE') {
        return Response('{}', 200);
      } else {
        var model = SignInModel.fromJson(jsonDecode(request.body));
        var signUpModel = SignUpModel.fromJson(jsonDecode(request.body));
        if (model.username == 'fail') {
          throw Exception();
        } else if (model.username == 'socket') {
          throw SocketException('');
        } else if (model.username == 'timeout') {
          throw TimeoutException('');
        } else {
          return Future<Response>.delayed(Duration(seconds: 2))
              .then((value) => Response('{}', 200));
        }
      }
    });

    _repo = AuthenticationRepositoryImpl(
        remoteDataSource: AuthenticationRemoteDataSourceImpl(client: mockClient));

    _widget = MaterialApp(
      home: Injector(inject: GlobalState.injectDataMocks(mockClient), builder: (_) => MyApp()),
    );

    _widget2 = MaterialApp(
      home: Injector(inject: GlobalState.injectData(), builder: (_) => MyApp()),
    );

    _homeWidget = MaterialApp(
      home: Injector(inject: GlobalState.injectData(), builder: (_) => HomePage()),
    );

    _textWidget = MaterialApp(
      home: Injector(
          inject: GlobalState.injectData(),
          builder: (_) => Column(
                children: [
                  Caption(''),
                  Header(''),
                  Headline(''),
                ],
              )),
    );
  });

  testWidgets('Login page and register page', (WidgetTester tester) async {
    await tester.pumpWidget(_widget);
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.tap(find.text('Klik Disini'));
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.enterText(find.byKey(Key('registerEmailKey')), 'fauzi');
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.enterText(find.byKey(Key('registerUsernameKey')), '');
    await tester.enterText(find.byKey(Key('registerUsernameKey')), 'fauzi');
    await tester.enterText(find.byKey(Key('registerUsernameKey')), '');
    await tester.enterText(find.byKey(Key('registerUsernameKey')), 'fauzi');
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.tap(find.text('Login'));
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.enterText(find.byKey(Key('loginUsernameKey')), 'fauzi');
    await tester.enterText(find.byKey(Key('loginUsernameKey')), 'fauzi@gmail.com');
    await tester.pumpAndSettle();
    await tester.enterText(find.byKey(Key('loginPasswordKey')), '2');
    await tester.enterText(find.byKey(Key('loginPasswordKey')), '');
    await tester.enterText(find.byKey(Key('loginPasswordKey')), 'fauzi@gmail.com');
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.tap(find.byKey(Key('signIn')));
    await tester.pump();
    await tester.pumpAndSettle(Duration(seconds: 5));
  });

  testWidgets('Home page', (WidgetTester tester) async {
    Pref.getStream.setBool('login', false);
    Pref.saveString('todoListCacheKey', DateTime.now().toIso8601String());
    await tester.pumpWidget(_widget2);
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.enterText(find.byKey(Key('loginUsernameKey')), 'fauzi');
    await tester.enterText(find.byKey(Key('loginUsernameKey')), 'fauzi@gmail.com');
    await tester.pumpAndSettle();
    await tester.enterText(find.byKey(Key('loginPasswordKey')), '');
    await tester.enterText(find.byKey(Key('loginPasswordKey')), '2');
    await tester.enterText(find.byKey(Key('loginPasswordKey')), 'fauzi@gmail.com');
    await tester.pumpAndSettle(Duration(seconds: 5));
    await tester.tap(find.text('Sign in with Google'));
    await tester.pump();
    await tester.tap(find.byKey(Key('obscureLogin')));
    await tester.pump();
    await tester.tap(find.byKey(Key('signIn')));
    await tester.pumpAndSettle(Duration(seconds: 5));
    GlobalState.theme().setState((s) => s.init(), silent: true);
  });

  testWidgets('Home page', (WidgetTester tester) async {
    await tester.pumpWidget(_textWidget);
    await tester.pumpAndSettle();
  });

  testWidgets('Home page', (WidgetTester tester) async {
    await tester.pumpWidget(_homeWidget);
    GlobalState.todoList().state.getTodoList;
    await tester.pump();
    await tester.pumpAndSettle();
  });

  testWidgets('Home page', (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({'login': false});
    await tester.pumpWidget(_homeWidget);
    await tester.pump();
    await tester.pumpAndSettle();
  });

  test('test sign in', () async {
    //arrange
    Pref.saveToken('token');
    Pref.saveString('token', 'test');
    Pref.getString('token');
    Pref.saveBool('token', true);
    Pref.getBool('token');
    Pref.getHeaderToken();
    //act
    SignInModel model = SignInModel(username: 'mocked', password: 'supermock');
    var resp = await _repo.signIn(model);
    resp.fold((failure) => print(failure), (result) => expect(result.statusCode, 200));
    //assert
  });

  test('sign in then throw error', () async {
    //arrange
    SignInModel model = SignInModel(username: 'fail', password: 'supermock');
    SignInModel modelSocket = SignInModel(username: 'socket', password: 'supermock');
    SignInModel modelTimeout = SignInModel(username: 'timeout', password: 'supermock');
    //act
    var resp = await _repo.signIn(model);
    var respSocket = await _repo.signIn(modelSocket);
    var respTimeout = await _repo.signIn(modelTimeout);
    //assert
    resp.fold((failure) => null, (result) => expect(result.statusCode, 200));
    respSocket.fold((failure) => expect(failure, isA<NetworkFailure>()), (result) => null);
    respTimeout.fold((failure) => expect(failure, isA<TimeoutFailure>()), (result) => null);
  });

  test('sign up ', () async {
    //arrange
    SignUpModel model = SignUpModel(username: 'mocked', password: 'supermock');
    //act
    var resp = await _repo.signUp(model);
    //assert
    resp.fold((failure) => null, (result) => expect(result.statusCode, 200));
  });

  test('Garbage', () async {
    TodoList model = TodoList(id: 1, title: 'empty', description: 'mocked');
  });
}
