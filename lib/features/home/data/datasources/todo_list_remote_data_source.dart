part of '_datasources.dart';

abstract class TodoListRemoteDataSource {
  Future<Parsed<String>> createTodoList(TodoListModel model, int accountId);
  Future<Parsed<List<TodoListModel>>> getTodoList(int accountId);
  Future<Parsed<String>> deleteTodoList(int todoListId);
}

class TodoListRemoteDataSourceImpl extends TodoListRemoteDataSource {
  @override
  Future<Parsed<String>> createTodoList(TodoListModel model, int accountId) async {
    var url = '${Endpoints.baseUrl}/todolist/$accountId';
    var resp = await postIt(url, model.toJson());
    return resp.parse('');
  }

  @override
  Future<Parsed<List<TodoListModel>>> getTodoList(int accountId) async {
    var url = '${Endpoints.baseUrl}/todolist/get-all/$accountId';
    var resp = await getIt(url);
    var listData = <TodoListModel>[];
    // if (resp.bodyAsMap['data'])
    //   for (var data in resp.bodyAsMap['data']) {
    //     listData.add(TodoListModel.fromJson(data));
    //   }
    return resp.parse(listData);
  }

  @override
  Future<Parsed<String>> deleteTodoList(int todoListId) async {
    var url = '${Endpoints.baseUrl}/todolist/$todoListId';
    var resp = await deleteIt(url);
    return resp.parse('');
  }
}
