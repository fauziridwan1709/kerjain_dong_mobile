part of '_models.dart';

class TodoListModel extends TodoList {
  TodoListModel({
    int id,
    String title,
    String description,
  }) : super(
          id: id,
          title: title,
          description: description,
        );

  TodoListModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    return data;
  }
}
