part of '_repositories.dart';

class TodoListRepositoryImpl implements TodoListRepository {
  final TodoListRemoteDataSource remoteDataSource = TodoListRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<String>>> createTodoList(TodoListModel model, int accountId) async {
    return await apiCall(remoteDataSource.createTodoList(model, accountId));
  }

  @override
  Future<Decide<Failure, Parsed<List<TodoList>>>> getTodoList(int accountId) async {
    return await apiCall(remoteDataSource.getTodoList(accountId));
  }

  @override
  Future<Decide<Failure, Parsed<String>>> deleteTodoList(int todoListId) async {
    return await apiCall(remoteDataSource.deleteTodoList(todoListId));
  }
}
