part of '_entities.dart';

class TodoList {
  int id;
  String title;
  String description;

  TodoList({this.id, this.title, this.description});
}
