part of '_repositories.dart';

abstract class TodoListRepository {
  Future<Decide<Failure, Parsed<String>>> createTodoList(TodoListModel model, int accountId);
  Future<Decide<Failure, Parsed<List<TodoList>>>> getTodoList(int accountId);
  Future<Decide<Failure, Parsed<String>>> deleteTodoList(int todoListId);
}
