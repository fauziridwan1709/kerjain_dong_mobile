part of '_pages.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends BaseState<HomePage, TodoListState> with TickerProviderStateMixin {
  bool _isOpenDrawer = false;
  AnimationController _controller;

  @override
  void init() {
    _controller = AnimationController(duration: Duration(milliseconds: 600), vsync: this);
  }

  @override
  Future<void> retrieveData() async {
    GlobalState.todoList().setState((s) => s.retrieveData(0));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute(
      backgroundColor: KerjainDongColors.darkBlue,
    );
  }

  @override
  Widget buildAppBar(BuildContext context) {
    // return AppBar();
    return PreferredSize(
        child: SafeArea(child: SizedBox.shrink()), preferredSize: Size.fromHeight(0));
  }

  @override
  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<TodoListState> snapshot, SizingInformation sizeInfo) {
    var width = sizeInfo.screenSize.width;
    var height = sizeInfo.screenSize.height;
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              HeightSpace(200),
              Row(
                children: [
                  AutoLayoutButton(
                    textColor: KerjainDongColors.darkBlue,
                    text: 'Sign Out',
                    backGroundColor: KerjainDongColors.white,
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    radius: BorderRadius.circular(6),
                    onTap: () {
                      Pref.getStream.setBool('login', false);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
        SlideTransition(
          position: Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(0.5, 0.0))
              .animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOutCubic)),
          child: ScaleTransition(
            scale: Tween<double>(begin: 1.0, end: .7)
                .animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOutCubic)),
            child: AnimatedContainer(
              height: height,
              width: width,
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.circular(_isOpenDrawer ? 20.0 : 0.0),
                  boxShadow: [KerjainDongColors.darkBoxShadow]),
              duration: Duration(milliseconds: 700),
              child: RefreshIndicator(
                onRefresh: retrieveData,
                key: refreshIndicatorKey,
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 40, vertical: 60),
                  children: [
                    Row(
                      children: <Widget>[
                        InkWell(
                            onTap: () {
                              setState(() => _isOpenDrawer = !_isOpenDrawer);
                              if (_isOpenDrawer) {
                                _controller.forward();
                              } else {
                                _controller.reverse();
                              }
                            },
                            child: SvgPicture.asset('assets/images/svg/drawer.svg')),
                      ],
                    ),
                    HeightSpace(40),
                    Text(
                      'Whats up, Paw',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    HeightSpace(10),
                    Text(
                      'Todo List',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    HeightSpace(10),
                    SizedBox(
                      height: 120,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        clipBehavior: Clip.none,
                        children: [
                          Container(
                            width: 160,
                            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                            margin: EdgeInsets.fromLTRB(0, 5, 8, 5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(14),
                              boxShadow: [KerjainDongColors.lightBoxShadow],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  '40 Tasks',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  'Fasilkom',
                                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                ),
                                HeightSpace(5),
                                Stack(
                                  children: [
                                    Container(
                                      height: 3,
                                      decoration: BoxDecoration(
                                        color: KerjainDongColors.disableColor,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                    Container(
                                      width: 80,
                                      height: 3,
                                      decoration: BoxDecoration(
                                          color: KerjainDongColors.lightBlue,
                                          borderRadius: BorderRadius.circular(20),
                                          boxShadow: [KerjainDongColors.lightBoxShadow]),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: 160,
                            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                            margin: EdgeInsets.fromLTRB(8, 5, 5, 5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(14),
                              boxShadow: [KerjainDongColors.lightBoxShadow],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  '40 Tasks',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  'Mantap',
                                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                ),
                                HeightSpace(5),
                                Stack(
                                  children: [
                                    Container(
                                      height: 3,
                                      decoration: BoxDecoration(
                                        color: KerjainDongColors.disableColor,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    ),
                                    Container(
                                      width: 80,
                                      height: 3,
                                      decoration: BoxDecoration(
                                          color: KerjainDongColors.accent,
                                          borderRadius: BorderRadius.circular(20),
                                          boxShadow: [KerjainDongColors.lightBoxShadow]),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    HeightSpace(20),
                    Text(
                      "Today's Tasks",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    HeightSpace(10),
                    Column(
                      children: [
                        'Check Emails',
                        'Check Emails',
                        'aduh gimana?',
                        'check mantap',
                        'aduh gomana?'
                      ].map((e) {
                        var isTwo = e == 'Check Emails';
                        return Container(
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                          margin: EdgeInsets.symmetric(vertical: 8),
                          decoration: BoxDecoration(
                            color: KerjainDongColors.white,
                            boxShadow: [KerjainDongColors.lightBoxShadow],
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: isTwo
                                          ? KerjainDongColors.accent
                                          : KerjainDongColors.lightBlue,
                                      width: 2),
                                  shape: BoxShape.circle,
                                ),
                              ),
                              WidthSpace(20),
                              Text(
                                e,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget buildWideLayout(
      BuildContext context, ReactiveModel<TodoListState> snapshot, SizingInformation sizeInfo) {
    // TODO: implement buildWideLayout
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }
}
