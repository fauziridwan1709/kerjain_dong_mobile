part of '_states.dart';

class TodoListState implements FutureState<TodoListState, int> {
  List<TodoList> _todoList;

  List<TodoList> get getTodoList => _todoList;

  @override
  String cacheKey = CacheKey.todoListCacheKey;

  @override
  bool getCondition() {
    return _todoList != null;
  }

  @override
  Future<void> retrieveData(int k) {}
}
