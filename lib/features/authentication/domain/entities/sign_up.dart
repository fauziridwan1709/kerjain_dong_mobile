part of '_entities.dart';

class SignUp {
  String username;
  String email;
  String password;

  SignUp({this.username, this.email, this.password});
}
