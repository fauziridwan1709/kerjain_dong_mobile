part of '_repositories.dart';

abstract class AuthenticationRepository {
  Future<Decide<Failure, Parsed<String>>> signIn(SignInModel model);
  Future<Decide<Failure, Parsed<String>>> signUp(SignUpModel model);
}
