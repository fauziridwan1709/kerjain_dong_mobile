part of '_datasources.dart';

abstract class AuthenticationRemoteDataSource {
  Future<Parsed<String>> signIn(SignInModel model);
  Future<Parsed<String>> signUp(SignUpModel model);
}

class AuthenticationRemoteDataSourceImpl extends AuthenticationRemoteDataSource {
  final Client client;
  AuthenticationRemoteDataSourceImpl({this.client});
  @override
  Future<Parsed<String>> signIn(SignInModel model) async {
    var url = '${Endpoints.auth}';
    var resp = await postIt(
      url,
      model.toJson(),
      client: client,
    );
    return resp.parse('');
  }

  @override
  Future<Parsed<String>> signUp(SignUpModel model) async {
    var url = '${Endpoints.auth}';
    var resp = await putIt(url, model.toJson(), client: client);
    var resp2 = await getIt(url, client: client);
    var resp3 = await deleteIt(url, client: client);
    return resp.parse('');
  }
}
