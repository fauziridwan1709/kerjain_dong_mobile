part of '_repositories.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationRemoteDataSource remoteDataSource;

  AuthenticationRepositoryImpl({@required this.remoteDataSource});

  @override
  Future<Decide<Failure, Parsed<String>>> signIn(SignInModel model) async {
    return await apiCall(remoteDataSource.signIn(model));
  }

  @override
  Future<Decide<Failure, Parsed<String>>> signUp(SignUpModel model) async {
    return await apiCall(remoteDataSource.signUp(model));
  }
}
