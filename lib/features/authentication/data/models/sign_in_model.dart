part of '_models.dart';

class SignInModel extends SignIn {
  SignInModel({
    String username,
    String password,
  }) : super(
          username: username,
          password: password,
        );

  SignInModel.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}
