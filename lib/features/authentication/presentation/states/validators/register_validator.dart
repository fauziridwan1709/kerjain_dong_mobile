part of '_validators.dart';

class RegisterValidator {
  final loginValidator = LoginValidator();
  final _fullNameController = BehaviorSubject<String>();

  Function(String) get onFullNameChanged => _fullNameController.sink.add;
  Function(String) get onEmailChanged => loginValidator.onEmailChanged;
  Function(String) get onPasswordChanged => loginValidator.onPasswordChanged;

  Stream<String> get validateFullName => _fullNameController.stream.transform(_validateFullName);
  Stream<String> get validateEmail => loginValidator.validateEmail;
  Stream<String> get validatePassword => loginValidator.validatePassword;

  final StreamTransformer<String, String> _validateFullName =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String inputString, EventSink<String> sink) {
    if (inputString.isEmpty) {
      sink.addError('Password cannot be empty!');
    } else {
      sink.add(inputString);
    }
  });

  Stream<bool> get isValid =>
      Rx.combineLatest3(validateEmail, validatePassword, validateFullName, (a, b, c) => true);

  void close() {
    loginValidator.close();
    _fullNameController.close();
  }
}
