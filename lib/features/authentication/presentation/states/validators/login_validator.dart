part of '_validators.dart';

const String kEmailRule =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

class LoginValidator {
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  Function(String) get onEmailChanged => _emailController.sink.add;
  Function(String) get onPasswordChanged => _passwordController.sink.add;
  Stream<String> get validateEmail => _emailController.stream.transform(_validateEmail);
  Stream<String> get validatePassword => _passwordController.stream.transform(_validatePassword);

  final StreamTransformer<String, String> _validateEmail =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String email, EventSink<String> sink) async {
    final RegExp emailExp = RegExp(kEmailRule);
    print(email);
    if (!emailExp.hasMatch(email) || email.isEmpty) {
      sink.addError('invalid email address format');
    } else {
      sink.add(email);
    }
  });

  final StreamTransformer<String, String> _validatePassword =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String password, EventSink<String> sink) {
    if (password.isEmpty) {
      sink.addError('Password cannot be empty!');
    } else if (password.length < 6) {
      sink.addError('Input atleast 6 characters');
    } else {
      sink.add(password);
    }
  });

  Stream<bool> get isValid => Rx.combineLatest2(validateEmail, validatePassword, (a, b) => true);

  void close() {
    _emailController.close();
    _passwordController.close();
  }
}
