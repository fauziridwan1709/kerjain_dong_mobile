import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:kerjain_dong/core/bases/widgets/_widgets.dart';
import 'package:kerjain_dong/core/extensions/_extensions.dart';
import 'package:kerjain_dong/core/utils/_utils.dart';
import 'package:kerjain_dong/features/authentication/data/datasources/_datasources.dart';
import 'package:kerjain_dong/features/authentication/data/models/_models.dart';
import 'package:kerjain_dong/features/authentication/data/repositories/_repositories.dart';
import 'package:kerjain_dong/features/authentication/domain/repositories/_repositories.dart';

part 'auth_state.dart';
