part of '_states.dart';

class AuthState {
  AuthenticationRepository repo;

  User _user;

  AuthState({Client client}) {
    AuthenticationRemoteDataSource dataSource = AuthenticationRemoteDataSourceImpl(client: client);
    repo = AuthenticationRepositoryImpl(remoteDataSource: dataSource);
  }

  Future<void> signIn(SignInModel model, BuildContext context) async {
    showBarrier(context);
    var result = await repo.signIn(model);
    result.fold<void>((failure) {
      context.pop();
      CustomFlushbar.failed(context, failure.toString());
      throw failure;
    }, (result) {
      if (result.statusCode == 200 || result.statusCode == 201) {
        context.pop();
        CustomFlushbar.succeed(context, 'Berhasil login');
        Pref.saveToken(result.data);
        Pref.getStream.setBool('login', true);
      }
    });
  }

  // Future<void> signOut() async {}
}
