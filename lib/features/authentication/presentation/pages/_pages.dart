import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kerjain_dong/core/bases/states/_states.dart';
import 'package:kerjain_dong/core/bases/widgets/_widgets.dart';
import 'package:kerjain_dong/core/bases/widgets/button/_button.dart';
import 'package:kerjain_dong/core/bases/widgets/core/_core.dart';
import 'package:kerjain_dong/core/bases/widgets/simple/_simple.dart';
import 'package:kerjain_dong/core/bases/widgets/text/_text.dart';
import 'package:kerjain_dong/core/screen/_screen.dart';
import 'package:kerjain_dong/core/theme/_theme.dart';
import 'package:kerjain_dong/core/utils/_utils.dart';
import 'package:kerjain_dong/features/authentication/data/models/_models.dart';
import 'package:kerjain_dong/features/authentication/presentation/states/validators/_validators.dart';
import 'package:kerjain_dong/features/authentication/presentation/widgets/_widgets.dart';
import 'package:kerjain_dong/features/home/presentation/pages/_pages.dart';
import 'package:logger/logger.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

// import 'package:url_launcher/url_launcher.dart';

part 'authentication_page.dart';
part 'login_page.dart';
part 'register_page.dart';
