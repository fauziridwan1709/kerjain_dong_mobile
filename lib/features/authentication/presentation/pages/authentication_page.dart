part of '_pages.dart';

class AuthenticationPage extends StatefulWidget {
  // AuthenticationPage({this.preferences});
  // final StreamingSharedPreferences preferences;

  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends BaseStateful<AuthenticationPage> {
  final auth = GlobalState.auth();
  bool _loginPage;

  @override
  void init() {
    _loginPage = true;
  }

  @override
  Widget buildAppBar(BuildContext context) {
    // TODO: implement buildAppBar
    return PreferredSize(child: SizedBox.shrink(), preferredSize: Size.fromHeight(0));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute(resize: true);
  }

  @override
  Widget buildNarrowLayout(BuildContext context, SizingInformation sizeInfo) {
    return PreferenceBuilder<bool>(
        preference: Pref.getStream.getBool('login', defaultValue: false),
        builder: (context, isLogin) {
          if (isLogin) {
            return HomePage();
          } else if (_loginPage) {
            return LoginPage(toogle: _changePage);
          }
          return RegisterPage(toogle: _changePage);
          // return Center(child: CircularProgressIndicator());
        });
  }

  ///tablet layout
  @override
  Widget buildWideLayout(BuildContext context, SizingInformation sizeInfo) {
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  void _changePage() {
    setState(() => _loginPage = !_loginPage);
  }
}
