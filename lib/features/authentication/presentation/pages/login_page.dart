part of '_pages.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback toogle;

  LoginPage({this.toogle});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with LoginValidator, SingleTickerProviderStateMixin {
  final auth = GlobalState.auth();

  FocusNode _fNodeEmail;
  FocusNode _fNodePassword;
  TextEditingController _emailController;
  TextEditingController _passwordController;
  AnimationController _controller;
  bool _isHidePassword;
  bool _isButtonLoading;

  @override
  void initState() {
    super.initState();
    _fNodeEmail = FocusNode();
    _fNodePassword = FocusNode();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _controller = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    _controller.forward();

    _isHidePassword = true;
    _isButtonLoading = false;
  }

  @override
  void dispose() {
    _controller.dispose();
    close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    print(Theme.of(context).backgroundColor);
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.bottomCenter,
          child: SimpleContainer(
            height: height,
            // height: 300,
            radius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24)),
            color: Theme.of(context).backgroundColor,
            child: Stack(
              children: [
                // FadeTransition(
                //   opacity: Tween<double>(begin: 0.1, end: 1.0).animate(
                //       CurvedAnimation(
                //           parent: _controller, curve: Curves.easeIn)),
                //   child: SimpleContainer(
                //     height: 310,
                //     // color: Theme.of(context).primaryColor,
                //     child: Align(
                //       alignment: Alignment(2.6, 0),
                //       child: Image.asset(
                //         'resources/images/png/login.png',
                //         width: 250,
                //       ),
                //     ),
                //   ),
                // ),
                ListView(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                  children: <Widget>[
                    HeightSpace(40),
                    SlideTransition(
                      position: Tween<Offset>(begin: Offset(0.0, -1.0), end: Offset(0.0, 0.0))
                          .animate(CurvedAnimation(parent: _controller, curve: Curves.bounceOut)),
                      child: Row(
                        children: [
                          Text(
                            'Kerjain Dong',
                            textAlign: TextAlign.left,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(fontSize: 24, fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                    ),
                    HeightSpace(25),
                    FadeTransition(
                      opacity: Tween<double>(begin: 0.1, end: 1.0)
                          .animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn)),
                      child: Text(
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s',
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                            fontSize: 14, fontWeight: FontWeight.w500, color: Colors.black54),
                      ),
                    ),
                    HeightSpace(15),
                    StreamBuilder<String>(
                        stream: validateEmail,
                        builder: (context, snapshot) {
                          return TextFieldWidget(
                            'Alamat Email',
                            'Ketikkan alamat email anda',
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                              child: SvgPicture.asset(
                                'assets/images/svg/email.svg',
                              ),
                            ),
                            key: Key('loginUsernameKey'),
                            fNode: _fNodeEmail,
                            controller: _emailController,
                            errorText: snapshot.error,
                            onChanged: onEmailChanged,
                          );
                        }),
                    StreamBuilder<String>(
                        stream: validatePassword,
                        builder: (context, snapshot) {
                          return TextFieldWidget(
                            'Kata Sandi',
                            'Ketikkan kata sandi anda',
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                              child: SvgPicture.asset(
                                'assets/images/svg/password.svg',
                              ),
                            ),
                            key: Key('loginPasswordKey'),
                            fNode: _fNodePassword,
                            controller: _passwordController,
                            errorText: snapshot.error,
                            onChanged: onPasswordChanged,
                            isHidePassword: _isHidePassword,
                            isPassword: true,
                            onPasswordObscureChange: () {
                              setState(() => _isHidePassword = !_isHidePassword);
                            },
                          );
                        }),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 15,
                      ),
                      child: InkWell(
                        onTap: widget.toogle,
                        child: Text(
                          'Lupa password?',
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                              color: Theme.of(context).primaryColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    HeightSpace(30),
                    StreamBuilder<bool>(
                        stream: isValid,
                        builder: (context, snapshot) {
                          if (_isButtonLoading) {
                            return Center(
                                child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),
                            ));
                          }
                          return AutoLayoutButton(
                            key: Key('signIn'),
                            text: 'Sign in',
                            margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                            textColor: KerjainDongColors.white,
                            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 6),
                            // color: Theme.of(context).primaryColor,
                            backGroundColor: Theme.of(context).primaryColor,
                            onTap: _handleOnLogin((snapshot?.data ?? false), context),

                            // onTap: () {
                            //   Navigator.pushReplacement(
                            //       context,
                            //       MaterialPageRoute(
                            //           builder: (context) => MainPage()));
                            // },
                            radius: BorderRadius.circular(12),
                          );
                        }),
                    HeightSpace(15),
                    AutoLayoutButton(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                      border: Border.all(color: Theme.of(context).primaryColor),
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 6),
                      type: ButtonType.widget,
                      backGroundColor: Colors.white,
                      onTap: () async {
                        // await auth.state.signInWithGoogle();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/png/google.png',
                            height: 20,
                            width: 20,
                          ),
                          WidthSpace(10),
                          Text(
                            'Sign in with Google',
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(color: Theme.of(context).primaryColor),
                          ),
                        ],
                      ),
                      radius: BorderRadius.circular(12),
                    ),
                    HeightSpace(10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Description('Belum punya akun? '),
                        InkWell(
                          onTap: widget.toogle,
                          child: Text(
                            'Klik Disini',
                            style: Theme.of(context).textTheme.bodyText1.copyWith(
                                color: Theme.of(context).primaryColor, fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  VoidCallback _handleOnLogin(bool goodCondition, BuildContext context) {
    if (goodCondition) {
      return () {
        setState(() => _isButtonLoading = true);
        if (_fNodeEmail.hasFocus || _fNodePassword.hasFocus) {
          _fNodeEmail.unfocus();
          _fNodePassword.unfocus();
        }
        SignInModel model = SignInModel(username: 'Muhamad Fauzi Ridwan ', password: '12345678');
        auth.setState((s) => s.signIn(model, context),
            onError: (error, context) {
              Logger().d(error);
              setState(() => _isButtonLoading = false);
            },
            onData: (state, context) => setState(() => _isButtonLoading = false));
      };
    }
    return null;
  }
}
