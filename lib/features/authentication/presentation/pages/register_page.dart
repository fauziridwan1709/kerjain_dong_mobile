part of '_pages.dart';

class RegisterPage extends StatefulWidget {
  final VoidCallback toogle;

  RegisterPage({this.toogle});

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage>
    with RegisterValidator, TickerProviderStateMixin {
  FocusNode _fNodeFullName;
  FocusNode _fNodeEmail;
  FocusNode _fNodePassword;
  TextEditingController _fullNameController;
  TextEditingController _emailController;
  TextEditingController _passwordController;
  AnimationController _controller;
  bool _isEmailExists;
  bool _isButtonLoading;
  bool _isHidePassword;

  @override
  void initState() {
    super.initState();
    _fNodeFullName = FocusNode();
    _fNodeEmail = FocusNode();
    _fNodePassword = FocusNode();
    _fullNameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _controller = AnimationController(duration: Duration(milliseconds: 1000), vsync: this);
    _controller.forward();
    _isEmailExists = false;
    _isButtonLoading = false;
    _isHidePassword = true;
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _controller.dispose();
    close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        // SimpleContainer(
        //   height: 400,
        //   color: Theme.of(context).primaryColor,
        //   child: Center(
        //     child: Image.asset(
        //       'resources/images/png/register.png',
        //       width: 150,
        //     ),
        //   ),
        // ),
        Align(
          alignment: Alignment.bottomCenter,
          child: SimpleContainer(
            height: height,
            // height: 300,
            radius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24)),
            color: Theme.of(context).backgroundColor,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              children: <Widget>[
                HeightSpace(40),
                SlideTransition(
                  position: Tween<Offset>(begin: Offset(0.0, -1.0), end: Offset(0.0, 0.0))
                      .animate(CurvedAnimation(parent: _controller, curve: Curves.bounceOut)),
                  child: Row(
                    children: [
                      Text(
                        'Kerjain Dong',
                        textAlign: TextAlign.left,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontSize: 24, fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                ),
                HeightSpace(25),
                FadeTransition(
                  opacity: Tween<double>(begin: 0.1, end: 1.0)
                      .animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn)),
                  child: Text(
                    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s',
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.black54),
                  ),
                ),
                HeightSpace(10),
                StreamBuilder<String>(
                    stream: validateFullName,
                    builder: (context, snapshot) {
                      return TextFieldWidget(
                        'Nama',
                        'Ketikkan nama anda',
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                          child: SvgPicture.asset(
                            'assets/images/svg/email.svg',
                          ),
                        ),
                        key: Key('registerUsernameKey'),
                        fNode: _fNodeFullName,
                        controller: _fullNameController,
                        errorText: snapshot.error,
                        onChanged: onFullNameChanged,
                      );
                    }),
                StreamBuilder<String>(
                    stream: validateEmail,
                    builder: (context, snapshot) {
                      return TextFieldWidget(
                        'Alamat Email',
                        'Ketikkan alamat email anda',
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                          child: SvgPicture.asset(
                            'assets/images/svg/email.svg',
                          ),
                        ),
                        key: Key('registerEmailKey'),
                        fNode: _fNodeEmail,
                        controller: _emailController,
                        errorText: _isEmailExists ? 'Email telah digunakan' : snapshot.error,
                        onChanged: (val) async {
                          onEmailChanged(val);
                          final RegExp emailExp = RegExp(kEmailRule);
                          setState(() => _isEmailExists = true);
                          // if (snapshot.hasData || emailExp.hasMatch(val)) {
                          //   var resp = await FirebaseFirestore.instance.doc('users/$val').get();
                          //   print(_isEmailExists);
                          //   setState(() => _isEmailExists = resp.exists);
                          // }
                        },
                        suffixIcon:
                            !_isEmailExists ? Icon(Icons.check_circle, color: Colors.green) : null,
                      );
                    }),
                StreamBuilder<String>(
                    stream: validatePassword,
                    builder: (context, snapshot) {
                      return TextFieldWidget(
                          'Kata Sandi',
                          'Ketikkan kata sandi anda',
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                            child: SvgPicture.asset(
                              'assets/images/svg/email.svg',
                            ),
                          ),
                          key: Key('registerPasswordKey'),
                          onPasswordObscureChange: () =>
                              setState(() => _isHidePassword = !_isHidePassword),
                          isHidePassword: _isHidePassword,
                          fNode: _fNodePassword,
                          controller: _passwordController,
                          errorText: snapshot.error,
                          onChanged: onPasswordChanged,
                          isPassword: true);
                    }),
                HeightSpace(15),
                StreamBuilder<bool>(
                    stream: isValid,
                    builder: (context, snapshot) {
                      if (_isButtonLoading) {
                        return Center(
                            child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),
                        ));
                      }
                      return AutoLayoutButton(
                        text: 'Sign up',
                        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                        textColor: KerjainDongColors.white,
                        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 6),
                        // color: Theme.of(context).primaryColor,
                        backGroundColor: Theme.of(context).primaryColor,
                        onTap: ((snapshot?.data ?? false) && !_isEmailExists)
                            ? () {
                                setState(() => _isButtonLoading = true);
                                // loginWithEmail(_emailController.text, _passwordController.text);
                              }
                            : null,
                        radius: BorderRadius.circular(12),
                      );
                    }),
                HeightSpace(15),
                AutoLayoutButton(
                  margin: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                  border: Border.all(color: Theme.of(context).primaryColor),
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 6),
                  type: ButtonType.widget,
                  backGroundColor: Colors.white,
                  onTap: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/png/google.png',
                        height: 20,
                        width: 20,
                      ),
                      WidthSpace(10),
                      Text(
                        'Sign up with Google',
                        style: Theme.of(context)
                            .textTheme
                            .button
                            .copyWith(color: Theme.of(context).primaryColor),
                      ),
                    ],
                  ),
                  radius: BorderRadius.circular(12),
                ),
                HeightSpace(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Description('Sudah punya akun? '),
                    InkWell(
                      onTap: widget.toogle,
                      child: Text(
                        'Login',
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                            color: Theme.of(context).primaryColor, fontWeight: FontWeight.w700),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
