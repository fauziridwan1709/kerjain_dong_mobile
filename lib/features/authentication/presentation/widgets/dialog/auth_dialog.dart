part of '../_widgets.dart';

// class AuthDialog extends StatelessWidget {
//   final String assetImage;
//   final String title;
//   final String description;
//   final List<String> actions;
//
//   const AuthDialog({Key key, this.assetImage, this.title, this.description, this.actions})
//       : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     Color primary = Theme.of(context).primaryColor;
//     var _actions = actions ?? <String>[];
//     return Dialog(
//       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
//       child: Padding(
//         padding: const EdgeInsets.fromLTRB(20, 20, 20, 25),
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             HeightSpace(10),
//             Image.asset(
//               assetImage,
//               height: 150,
//               width: 150,
//             ),
//             Text(
//               title,
//               textAlign: TextAlign.center,
//               style: Theme.of(context)
//                   .textTheme
//                   .bodyText1
//                   .copyWith(color: primary, fontSize: 20, fontWeight: FontWeight.bold),
//             ),
//             HeightSpace(15),
//             Text(
//               description,
//               textAlign: TextAlign.center,
//               style:
//                   Theme.of(context).textTheme.caption.copyWith(color: Colors.black87, fontSize: 14),
//             ),
//             HeightSpace(30),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: <Widget>[
//                 Expanded(
//                   flex: 1,
//                   child: SimpleContainer(
//                     onTap: () => Navigator.pop(context, false),
//                     color: UinfoColors.transparent,
//                     padding: EdgeInsets.symmetric(horizontal: 5, vertical: 3),
//                     radius: BorderRadius.circular(100),
//                     border: Border.all(color: primary, width: 1.5),
//                     child: Center(
//                       child: Text(
//                         _actions.first ?? 'No',
//                         style: Theme.of(context)
//                             .textTheme
//                             .bodyText1
//                             .copyWith(color: primary, fontSize: 14),
//                       ),
//                     ),
//                   ),
//                 ),
//                 WidthSpace(10),
//                 Expanded(
//                   flex: 1,
//                   child: SimpleContainer(
//                     onTap: () => Navigator.pop(context, true),
//                     color: primary,
//                     padding: EdgeInsets.symmetric(horizontal: 5, vertical: 3),
//                     radius: BorderRadius.circular(100),
//                     border: Border.all(color: primary, width: 1.5),
//                     child: Center(
//                         child: Text(
//                       _actions.last ?? 'Yes',
//                       style: Theme.of(context)
//                           .textTheme
//                           .bodyText1
//                           .copyWith(color: UinfoColors.white, fontSize: 14),
//                     )),
//                   ),
//                 ),
//               ],
//             ),
//             HeightSpace(20)
//           ],
//         ),
//       ),
//     );
//   }
// }
