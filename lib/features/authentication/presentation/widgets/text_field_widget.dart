part of '_widgets.dart';

class TextFieldWidget extends StatelessWidget {
  final String label;
  final String hint;
  final Widget widget;
  final TextEditingController controller;
  final FocusNode fNode;
  final String errorText;
  final Function(String) onChanged;
  final bool isHidePassword;
  final bool isPassword;
  final bool isEmptyFill;
  final VoidCallback onPasswordObscureChange;
  final Widget suffixIcon;

  TextFieldWidget(
    this.label,
    this.hint,
    this.widget, {
    Key key,
    this.controller,
    this.fNode,
    this.errorText,
    this.onChanged,
    this.isHidePassword = false,
    this.isPassword = false,
    this.isEmptyFill = false,
    this.onPasswordObscureChange,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var color = Theme.of(context).primaryColor;
    // print(color);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 11),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Description(label),
          HeightSpace(5),
          TextField(
            style: Theme.of(context).textTheme.bodyText1,
            controller: controller,
            onChanged: onChanged,
            obscureText: isHidePassword,
            focusNode: fNode,
            decoration: InputDecoration(
              hintText: hint,
              hintStyle: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.grey),
              errorText: errorText,
              suffixIcon: isPassword
                  ? IconButton(
                      key: Key('obscureLogin'),
                      onPressed: onPasswordObscureChange,
                      icon: Icon(isHidePassword ? Icons.visibility : Icons.visibility_off),
                    )
                  : suffixIcon,
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(
                    color: isEmptyFill ? Theme.of(context).highlightColor : Colors.transparent),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(
                    color: isEmptyFill ? Theme.of(context).highlightColor : Colors.transparent),
              ),
              prefixIcon: widget,
              filled: true,
              fillColor:
                  isEmptyFill ? KerjainDongColors.transparent : Theme.of(context).highlightColor,
            ),
          )
        ],
      ),
    );
  }
}
