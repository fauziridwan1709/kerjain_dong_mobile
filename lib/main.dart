import 'package:flutter/material.dart';
import 'package:kerjain_dong/core/bases/states/_states.dart';
import 'package:kerjain_dong/core/utils/_utils.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

import 'features/authentication/presentation/pages/_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final preferences = await StreamingSharedPreferences.instance;
  await Pref.init(preferences: preferences);
  runApp(Injector(
    inject: GlobalState.injectData(),
    builder: (BuildContext context) {
      final theme = GlobalState.theme();
      theme.setState((s) => s.init(), silent: true);
      return MyApp();
    },
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: GlobalState.theme().state.type,
      home: AuthenticationPage(),
    );
  }
}
