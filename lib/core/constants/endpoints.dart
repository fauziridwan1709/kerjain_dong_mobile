part of '_constants.dart';

class Endpoints {
  static const baseUrl = 'https://kerjain-dong-api.herokuapp.com';
  static const todoList = '$baseUrl/todolist';
  static const auth = '$baseUrl/authenticate';
}
