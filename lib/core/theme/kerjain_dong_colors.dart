part of '_theme.dart';

class KerjainDongColors {
  static const red = Color(0xFFD64949);
  static const yellow = Color(0xFFF7C600);
  static const green = Color(0xFF19BA62);
  static const purple = Color(0xFF5C49D6);
  static const darkPurple = Color(0xFF633CAE);
  static const lightBlue = Color(0xFF2C78F6);
  static const accent = Color(0xFFDB3FF6);
  static const darkBlue = Color(0xFF0F255B);

  static const transparent = Color(0x00000000);
  static const disableColor = Color(0xFFAFAFAF);
  static const filledTextFieldColor = Color(0xFFE3E3E3);
  // static const backgroundColor = Color(0xFFF6F5FF);
  static const backgroundColor = Color(0xFFFFFFFF);
  static const iconColor = Color(0xFF0E0943);

  static const white = Colors.white;

  static const listThemeColor = <Color>[red, yellow, green, purple, darkPurple];

  static const darkBoxShadow =
      BoxShadow(color: Colors.black, spreadRadius: 4, blurRadius: 6, offset: Offset(1.0, 1.0));

  static const lightBoxShadow =
      BoxShadow(color: Color(0x11AFAFAF), spreadRadius: 4, blurRadius: 6, offset: Offset(1.0, 1.0));
}
