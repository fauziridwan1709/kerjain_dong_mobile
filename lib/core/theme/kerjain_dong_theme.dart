part of '_theme.dart';

class KerjainDongThemeImpl implements KerjainDongTheme {
  ThemeData _baseThemeData() {
    return ThemeData(
        backgroundColor: KerjainDongColors.backgroundColor,
        disabledColor: KerjainDongColors.disableColor,
        // brightness: Brightness.dark,
        brightness: Brightness.light,
        highlightColor: KerjainDongColors.filledTextFieldColor,
        iconTheme: IconThemeData(color: KerjainDongColors.iconColor, opacity: 1.0),
        textTheme: TextTheme(
          headline4: GoogleFonts.poppins(
              fontSize: 38.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic),
          headline5: TextStyle(
              debugLabel: 'englishLike headline 2014',
              inherit: false,
              fontSize: 24.0,
              fontWeight: FontWeight.w400,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic),
          headline6: GoogleFonts.poppins(
              fontSize: 20.0,
              color: Colors.black,
              fontWeight: FontWeight.w700,
              textBaseline: TextBaseline.alphabetic),
          bodyText1: GoogleFonts.poppins(
              fontSize: 14.0,
              color: Colors.black,
              fontWeight: FontWeight.w500,
              textBaseline: TextBaseline.alphabetic),
          bodyText2: GoogleFonts.poppins(
              fontSize: 14.0,
              color: Colors.black,
              fontWeight: FontWeight.w400,
              textBaseline: TextBaseline.alphabetic),
          subtitle1: TextStyle(
              debugLabel: 'englishLike subhead 2014',
              inherit: false,
              fontSize: 16.0,
              fontWeight: FontWeight.w400,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic),
          subtitle2: TextStyle(
              debugLabel: 'englishLike subtitle 2014',
              inherit: false,
              fontSize: 14.0,
              fontWeight: FontWeight.w500,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic,
              letterSpacing: 0.1),
          caption: TextStyle(
              debugLabel: 'englishLike caption 2014',
              inherit: false,
              fontSize: 12.0,
              fontWeight: FontWeight.w400,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic),
          button: GoogleFonts.poppins(
              fontSize: 14.0,
              fontWeight: FontWeight.w500,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic),
          overline: TextStyle(
              debugLabel: 'englishLike overline 2014',
              inherit: false,
              fontSize: 10.0,
              fontWeight: FontWeight.w400,
              color: Colors.black,
              textBaseline: TextBaseline.alphabetic,
              letterSpacing: 1.5),
        ));
  }

  @override
  ThemeData normalTheme() {
    return _baseThemeData().copyWith(primaryColor: KerjainDongColors.lightBlue);
  }

  @override
  ThemeData mapTheme(ThemeType type) {
    switch (type) {
      case ThemeType.normalTheme:
        return normalTheme();
        break;
      default:
        return normalTheme();
        break;
    }
  }
}
