part of '_theme.dart';

abstract class KerjainDongTheme {
  ThemeData normalTheme();
  ThemeData mapTheme(ThemeType type);
}
