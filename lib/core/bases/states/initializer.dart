part of '_states.dart';

enum InitializerDelay {
  min1,
  min5,
}

///initializer with refresh indicatorKey
class Initializer {
  ReactiveModel rm;
  String ck;
  GlobalKey<RefreshIndicatorState> rIndicator;
  bool state;
  InitializerDelay delay;

  Initializer(
      {@required ReactiveModel reactiveModel,
      @required String cacheKey,
      @required this.rIndicator,
      @required this.state,
      this.delay = InitializerDelay.min5})
      : assert(cacheKey != '' && cacheKey != null),
        rm = reactiveModel,
        ck = cacheKey;

  void initialize() {
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(ck)) {
        var date = value.getString(ck);
        if (_isDifferenceLessThanFiveMin(date) && state) {
        } else {
          await value.setString(ck, DateTime.now().toIso8601String());
          await rIndicator.currentState?.show();
        }
      } else {
        await value.setString(ck, DateTime.now().toIso8601String());
        await rIndicator.currentState?.show();
      }
    });
  }

  bool _isDifferenceLessThanFiveMin(String time) {
    return DateTime.now().difference(DateTime.parse(time)) < Duration(minutes: 5);
  }
}
