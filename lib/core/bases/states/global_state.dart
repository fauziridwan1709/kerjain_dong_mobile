part of '_states.dart';

class GlobalState {
  static List<Injectable> injectDataMocks(Client client) {
    return <Injectable>[
      Inject(() => ThemeState()),
      Inject(() => AuthState(client: client)),
      Inject(() => TodoListState()),
    ];
  }

  static List<Injectable> injectData() {
    return <Injectable>[
      Inject(() => ThemeState()),
      Inject(() => AuthState()),
      Inject(() => TodoListState()),
    ];
  }

  static ReactiveModel<ThemeState> theme() {
    return Injector.getAsReactive<ThemeState>();
  }

  static ReactiveModel<AuthState> auth() {
    return Injector.getAsReactive<AuthState>();
  }

  static ReactiveModel<TodoListState> todoList() {
    return Injector.getAsReactive<TodoListState>();
  }
}
