part of '_states.dart';

enum ThemeType {
  ///dark purple theme
  normalTheme,
}

class ThemeState {
  static const String THEME_KEY = 'theme';
  KerjainDongTheme theme = KerjainDongThemeImpl();
  ThemeData type;

  void init() {
    var themeType = _mapStringToThemeType(Pref.getString(THEME_KEY));
    type = theme.mapTheme(themeType);
    print(themeType);
  }

  // void changeTheme(ThemeType themeType) {
  //   Pref.saveString(THEME_KEY, _mapThemeTypeToString(themeType));
  //   type = theme.mapTheme(themeType);
  // }

  ThemeType _mapStringToThemeType(String type) {
    switch (type) {
      case null:
        return ThemeType.normalTheme;
        break;
      default:
        return ThemeType.normalTheme;
        break;
    }
  }

  // String _mapThemeTypeToString(ThemeType type) {
  //   switch (type) {
  //     case null:
  //       return 'normalTheme';
  //       break;
  //     default:
  //       return 'normalTheme';
  //       break;
  //   }
  // }
}
