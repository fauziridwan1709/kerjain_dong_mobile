part of '_widgets.dart';

class CustomFlushbar {
  static void succeed(BuildContext context, String message) {
    Flushbar(
      duration: Duration(seconds: 3),
      message: message,
      backgroundColor: Colors.green,
    ).show(context);
  }

  static void failed(BuildContext context, String message) {
    Flushbar(
      duration: Duration(seconds: 3),
      message: message,
      backgroundColor: Colors.red,
    ).show(context);
  }
}
