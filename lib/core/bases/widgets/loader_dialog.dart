part of '_widgets.dart';

Future<void> showBarrier(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext dialogContext) {
      return Center();
    },
  );
}

// Future<void> showLoader(BuildContext context) async {
//   return showDialog<void>(
//     context: context,
//     barrierDismissible: true,
//     builder: (BuildContext dialogContext) {
//       return Center(
//           child: SizedBox(
//         height: 35,
//         width: 35,
//         child: CircularProgressIndicator(
//             valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor)),
//       ));
//     },
//   );
// }

// Widget loading() {
//   return Center(
//     child: SpinKitCubeGrid(
//       color: GlobalState.theme().state.type.primaryColor,
//     ),
//   );
// }
