import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kerjain_dong/core/bases/states/_states.dart';
import 'package:kerjain_dong/core/bases/widgets/simple/_simple.dart';
import 'package:kerjain_dong/core/screen/_screen.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

part 'base_state.dart';
part 'base_state_normal.dart';
part 'base_state_rebuilder.dart';
part 'base_stateful.dart';
