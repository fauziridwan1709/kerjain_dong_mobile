part of '_core.dart';

abstract class BaseState<T extends StatefulWidget, K extends FutureState<K, dynamic>>
    extends State<T> with Diagnosticable implements BaseStateReBuilder<K> {
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;

  @override
  void initState() {
    super.initState();
    init();
    refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    ///checker
    Initializer(
      reactiveModel: Injector.getAsReactive<K>(),
      rIndicator: refreshIndicatorKey,
      state: Injector.getAsReactive<K>().state.getCondition(),
      cacheKey: Injector.getAsReactive<K>().state.cacheKey,
    ).initialize();
  }

  ScaffoldAttribute buildAttribute();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      // statusBarIconBrightness: Theme.of(context).brightness,
      statusBarIconBrightness: Brightness.dark,
      // statusBarBrightness: Theme.of(context).brightness,
      statusBarBrightness: Brightness.dark,
    ));

    return WillPopScope(
        onWillPop: onBackPressed,
        child: SimpleScaffold(
          attr: buildAttribute(),
          body: StateBuilder(
              observe: () => Injector.getAsReactive<K>(),
              builder: (context, ReactiveModel<K> snapshot) => SizingInformationBuilder(
                    builder: (context, sizeInfo) {
                      if (sizeInfo.deviceType == DeviceScreenType.mobile) {
                        return buildNarrowLayout(context, snapshot, sizeInfo);
                      }
                      return buildWideLayout(context, snapshot, sizeInfo);
                    },
                  )),
          appBar: buildAppBar(context),
        ));
  }
}
