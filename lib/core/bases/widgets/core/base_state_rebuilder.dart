part of '_core.dart';

abstract class BaseStateReBuilder<K extends FutureState<K, dynamic>> {
  void init();

  Widget buildAppBar(BuildContext context);

  // @deprecated
  // Widget buildBody(BuildContext context, ReactiveModel<K> snapshot);

  Widget buildNarrowLayout(
      BuildContext context, ReactiveModel<K> snapshot, SizingInformation sizeInfo);

  Widget buildWideLayout(
      BuildContext context, ReactiveModel<K> snapshot, SizingInformation sizeInfo);

  Future<void> retrieveData();

  @protected
  Future<bool> onBackPressed();
}
