part of '_button.dart';

class AutoLayoutButton extends StatelessWidget {
  final String text;
  final Color color;
  final Color textColor;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final Color backGroundColor;
  final BorderRadius radius;
  final BoxBorder border;
  final ButtonType type;
  final VoidCallback onTap;
  final double fontSize;
  final Widget child;

  AutoLayoutButton(
      {Key key,
      this.text,
      this.color,
      this.textColor,
      this.padding = const EdgeInsets.all(0),
      this.margin,
      this.backGroundColor,
      this.radius,
      this.border,
      this.type = ButtonType.text,
      this.onTap,
      this.fontSize,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SimpleContainer(
          padding: padding,
          margin: margin,
          radius: radius,
          color: onTap == null
              ? Theme.of(context).disabledColor
              : backGroundColor ?? Theme.of(context).primaryColor,
          onTap: onTap,
          border: border,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(.2),
                offset: Offset(0, 0),
                spreadRadius: 2,
                blurRadius: 3)
          ],
          child: type == ButtonType.text
              ? Center(
                  child: Text(
                    text,
                    style: Theme.of(context)
                        .textTheme
                        .button
                        .copyWith(color: textColor),
                    // TextStyle(
                    //     fontWeight: FontWeight.w600,
                    //     fontSize: fontSize,
                    //     color: textColor ),
                  ),
                )
              : child),
    );
  }
}
