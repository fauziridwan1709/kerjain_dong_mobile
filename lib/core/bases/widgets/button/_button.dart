import 'package:flutter/material.dart';
import 'package:kerjain_dong/core/bases/widgets/simple/_simple.dart';

part 'auto_layout_button.dart';

enum ButtonType {
  ///wrap text inside button
  text,

  ///wrap widget inside button
  widget,
}
