part of '_text.dart';

class Description extends StatelessWidget {
  final String text;
  final FontWeight weight;
  final Color color;
  final int maxLines;

  const Description(this.text, {this.weight, this.color, this.maxLines});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        maxLines: maxLines,
        overflow: TextOverflow.clip,
        style: Theme.of(context)
            .textTheme
            .bodyText1
            .copyWith(fontWeight: weight, color: color));
  }
}
