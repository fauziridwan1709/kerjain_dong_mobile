part of '_text.dart';

class Headline extends StatelessWidget {
  final String text;

  const Headline(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(fontWeight: FontWeight.w400));
  }
}
