part of '_text.dart';

class Header extends StatelessWidget {
  final String text;

  const Header(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: Theme.of(context).textTheme.headline4);
  }
}
