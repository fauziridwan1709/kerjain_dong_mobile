part of '_text.dart';

class Caption extends StatelessWidget {
  final String text;

  const Caption(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: Theme.of(context).textTheme.caption);
  }
}
