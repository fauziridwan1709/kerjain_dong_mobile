part of '_simple.dart';

class SimpleScaffold extends Scaffold {
  SimpleScaffold({ScaffoldAttribute attr, PreferredSizeWidget appBar, Widget body})
      : super(
            key: attr.scaffoldKey,
            resizeToAvoidBottomInset: attr.resize,
            backgroundColor: attr.backgroundColor,
            floatingActionButton: attr.FAB,
            floatingActionButtonLocation: attr.FABLocation,
            appBar: appBar,
            body: body);
}

class ScaffoldAttribute {
  GlobalKey<ScaffoldState> scaffoldKey;
  Color backgroundColor;
  Widget FAB;
  bool resize;
  FloatingActionButtonLocation FABLocation;

  ScaffoldAttribute(
      {this.scaffoldKey,
      this.resize = true,
      this.backgroundColor = KerjainDongColors.white,
      this.FAB,
      this.FABLocation});
}
