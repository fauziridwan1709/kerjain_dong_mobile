part of '_simple.dart';

class SimpleContainer extends InkWell {
  SimpleContainer(
      {double width,
      double height,
      Widget child,
      Color color,
      EdgeInsetsGeometry padding = const EdgeInsets.all(0),
      EdgeInsetsGeometry margin,
      BorderRadius radius,
      BoxBorder border,
      BoxShape shape = BoxShape.rectangle,
      VoidCallback onTap,
      AlignmentGeometry align,
      DecorationImage image,
      List<BoxShadow> boxShadow})
      : super(
            onTap: onTap,
            child: Container(
                width: width,
                height: height,
                margin: margin,
                padding: padding,
                alignment: align,
                decoration: BoxDecoration(
                    shape: shape,
                    color: color,
                    image: image,
                    borderRadius: radius,
                    boxShadow: boxShadow,
                    border: border),
                child: Padding(padding: padding, child: child)));
}
