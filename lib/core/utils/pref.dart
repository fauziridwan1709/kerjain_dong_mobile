part of '_utils.dart';

///singleton class
class Pref {
  static SharedPreferences _pref;
  static StreamingSharedPreferences getStream;

  static Future<void> init({StreamingSharedPreferences preferences}) async {
    _pref = await SharedPreferences.getInstance();
    getStream = preferences;
    print('init');
  }

  static saveToken(String $token) async {
    await _pref.setString('Authorization', $token);
  }

  static saveString(String key, String value) async {
    await _pref.setString(key, value);
  }

  static saveBool(String key, bool value) async {
    await _pref.setBool(key, value);
  }

  static String getString(String key) {
    return _pref.getString(key);
  }

  static bool getBool(String key) {
    return _pref.getBool(key);
  }

  static Map<String, String> getHeaderToken() {
    return {'Authorization': _pref.getString('Authorization'), 'Content-Type': 'application/json'};
  }
}
