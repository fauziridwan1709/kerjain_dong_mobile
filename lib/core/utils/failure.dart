part of '_utils.dart';

abstract class Decide<T, K> {
  const Decide();
  B fold<B>(B ifLeft(T failure), B ifRight(K result));
}

class Left<L, R> extends Decide<L, R> {
  final L _l;
  const Left(this._l);

  @override
  B fold<B>(B Function(L t) ifLeft, B Function(R k) ifRight) => ifLeft(_l);
}

class Right<L, R> extends Decide<L, R> {
  final R _r;
  const Right(this._r);

  @override
  B fold<B>(B Function(L t) ifLeft, B Function(R k) ifRight) => ifRight(_r);
}

abstract class Failure implements Exception {}

class NetworkFailure extends Failure {}

class NotFoundFailure extends Failure {}

class BadRequestFailure extends Failure {}

class GeneralFailure extends Failure {
  String message;
  GeneralFailure(this.message);
}

class TimeoutFailure extends Failure {}
