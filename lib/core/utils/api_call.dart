part of '_utils.dart';

Future<Decide<Failure, T>> apiCall<T>(Future<T> t) async {
  try {
    var futureCall = await t;
    return Right(futureCall);
  } on SocketException {
    return Left(NetworkFailure());
  } on TimeoutException {
    return Left(TimeoutFailure());
  } catch (e) {
    return Left(GeneralFailure(''+e.toString()));
  }
}