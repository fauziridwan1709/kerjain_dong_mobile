part of '_utils.dart';

const GLOBAL_TIMEOUT = Duration(seconds: 15);

Future<Response> getIt(String url, {Map<String, String> headers, Client client}) async {
  var getHeaders = headers ?? Pref.getHeaderToken();
  var resp = await (client ?? Client()).get(url, headers: getHeaders);
  Logger().d(url);
  Logger().d(getHeaders);
  Logger().d(resp);
  return resp;
}

Future<Response> postIt(String url, Map<String, dynamic> reqBody,
    {Map<String, String> headers, Client client}) async {
  var getHeaders = headers ?? Pref.getHeaderToken();
  var resp = await (client ?? Client())
      .post(url, headers: getHeaders, body: json.encode(reqBody))
      .timeout(GLOBAL_TIMEOUT);
  Logger().d(url);
  Logger().d(getHeaders);
  Logger().d(resp.body);
  Logger().d(resp.statusCode);
  return resp;
}

Future<Response> putIt(String url, Map<String, dynamic> reqBody,
    {bool isIdentity = false, Client client}) async {
  var getHeaders = Pref.getHeaderToken();
  var resp = await (client ?? Client())
      .put(url, headers: getHeaders, body: json.encode(reqBody))
      .timeout(GLOBAL_TIMEOUT);
  Logger().d(url);
  Logger().d(getHeaders);
  Logger().d(resp.body);
  return resp;
}

Future<Response> deleteIt(String url, {Client client}) async {
  var getHeaders = Pref.getHeaderToken();
  var resp = await (client ?? Client()).delete(url, headers: getHeaders).timeout(GLOBAL_TIMEOUT);
  Logger().d(url);
  Logger().d(getHeaders);
  Logger().d(resp.body);
  return resp;
}
