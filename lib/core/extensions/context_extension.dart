part of '_extensions.dart';

extension ContextExtension on BuildContext {
  // Future<void> push(Widget widget) =>
  //     Navigator.push<void>(this, MaterialPageRoute(builder: (_) => widget));
  //
  // Future<void> pushReplacement(Widget widget) =>
  //     Navigator.pushReplacement<void, void>(this, MaterialPageRoute(builder: (_) => widget));

  void pop() => Navigator.pop(this);
}
