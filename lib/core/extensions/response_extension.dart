part of '_extensions.dart';

class Parsed<T> {
  int statusCode;
  String message;
  T data;

  Parsed.fromJson(Map<String, dynamic> json, int statusCode, T type) {
    this.statusCode = statusCode;
    message = json['message'] ?? json['status'];
    data = type;
  }
}

extension ResponseExtension<T> on Response {
  // ignore: avoid_shadowing_type_parameters
  Parsed<T> parse<T>(T t) {
    print('hoy2');
    print(t);
    return Parsed.fromJson(json.decode(body) as Map<String, dynamic>, statusCode, t);
  }

  // Map<String, dynamic> get bodyAsMap => json.decode(body) as Map<String, dynamic>;

  // Map<String, dynamic> get dataBodyAsMap => (json.decode(body) as Map<String, dynamic>)['data'];
}
